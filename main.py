import xml.etree.ElementTree as ET
from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np

tree = ET.parse('export.xml')
root = tree.getroot()

raw_adimlar = dict()
kalp_atisi = dict()

for child in root:
    try:
        # Gunluk toplam adim sayimi bulalim
        if child.attrib["type"] == "HKQuantityTypeIdentifierStepCount":
            start_day = datetime.strptime(child.attrib["startDate"], '%Y-%m-%d %H:%M:%S %z').strftime('%Y-%m-%d')
            end_day = datetime.strptime(child.attrib["endDate"], '%Y-%m-%d %H:%M:%S %z').strftime('%Y-%m-%d')
            if start_day == end_day:
                try:
                    raw_adimlar[start_day] += int(child.attrib["value"])
                except:
                    raw_adimlar[start_day] = int(child.attrib["value"])
        # gunluk kalp atislarimi bulalim
        if child.attrib["type"] == "HKQuantityTypeIdentifierHeartRate":
            start_day = datetime.strptime(child.attrib["startDate"], '%Y-%m-%d %H:%M:%S %z').strftime('%Y-%m-%d')
            end_day = datetime.strptime(child.attrib["endDate"], '%Y-%m-%d %H:%M:%S %z').strftime('%Y-%m-%d')
            if start_day == end_day:
                try:
                    kalp_atisi[start_day].append(int(child.attrib["value"]))
                except:
                    kalp_atisi[start_day] = [int(child.attrib["value"])]
    except:
        pass

adimlar = {k: v for k, v in raw_adimlar.items()
           if v > 1000}

gunluk_adim_ortalama = 0

for key, value in adimlar.items():
    gunluk_adim_ortalama = np.mean(value)

print(gunluk_adim_ortalama)

print(f"Toplam {len(raw_adimlar)} gune ait adim sayisi bulundu")
print(f"Toplam {len(adimlar)} gune ait adim sayisi gecerli sayildi")
print(f"Bu {len(adimlar)} tane verinin ortalamasi {gunluk_adim_ortalama} adimdir")
# https://www.health.harvard.edu/blog/10000-steps-a-day-or-fewer-2019071117305

yetersiz_gunler = np.sum(np.array(list(adimlar.values())) < 7500)

print(f"Toplam {yetersiz_gunler} gun, onerilen 7500 adimdan az adim atmisim")

sorted_adimlar = sorted(adimlar.items(), key=lambda x: x[1], reverse=True)

print(
    f"En cok adim attigim ilk 3 gun: {sorted_adimlar[0][0]} - {sorted_adimlar[0][1]} adim, {sorted_adimlar[1][0]} - {sorted_adimlar[1][1]} adim, {sorted_adimlar[2][0]} - {sorted_adimlar[2][1]} adim")

# bu verileri grafik olarak cizdirelim

plt.figure(figsize=(18, 6))
x_adimlar, y_adimlar = zip(*adimlar.items())
plt.bar(x_adimlar, y_adimlar)
plt.title('Yusuf\'un Adim Sayisi')
plt.xlabel('Gunler')
plt.ylabel('Adimlar')
plt.xlim(left=0)
plt.margins(x=0)
plt.autoscale()
plt.gca().get_xaxis().set_visible(False)

plt.show()

# gun icinde cok fazla deger oldugu icin ortalamasini alalim
kalp_atisi_gunluk_ortalama = {}
for key, value in kalp_atisi.items():
    kalp_atisi_gunluk_ortalama[key] = np.mean(value)

# tum gunlerin ortalamasini alalim
kalp_atisi_ortalama = 0

for key, value in kalp_atisi_gunluk_ortalama.items():
    kalp_atisi_ortalama = np.mean(value)

print(f"Toplam {len(kalp_atisi_gunluk_ortalama)} gune ait kalp atisi bulundu")
print(f"Bu {len(kalp_atisi_gunluk_ortalama)} tane verinin ortalamasi {kalp_atisi_ortalama} dir")

# bu verileri grafik olarak cizdirelim
x_kalpatisi, y_kalpatisi = zip(*kalp_atisi_gunluk_ortalama.items())

plt.figure(figsize=(18, 6))
plt.plot(x_kalpatisi, y_kalpatisi, linestyle='-', color='r', alpha=1)
plt.title('Yusuf\'un Gunluk Kalp Atis Grafigi')
plt.xlabel('Gunler')
plt.ylabel('Atislar')
plt.xlim(left=0)
plt.margins(x=0)
plt.autoscale()
plt.gca().get_xaxis().set_visible(False)

plt.show()

# bu iki veriyi ayni grafikte cizdirelim

fig = plt.figure(figsize=(18, 6))
ax1 = plt.subplot()
ax1.bar(x_adimlar, y_adimlar)
ax1.tick_params(axis='y')
ax2 = ax1.twinx()
ax2.plot(x_kalpatisi, y_kalpatisi, linestyle='-', color='r', alpha=0.5)
plt.xlim(left=0)
plt.margins(x=0)
ax1.get_xaxis().set_visible(False)
plt.title('Yusuf\'un Adim Sayisi ve Kalp Atisi Grafigi')
plt.xlabel('Gunler')
plt.ylabel('Kalp Atisi')
ax1.set_ylabel('Adimlar')

# Grafiği göster
plt.show()
